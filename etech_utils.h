/*

	Projet    : Module 'etech_utils'.
	Auteur    : © Philippe Maréchal
	Date      : 10 février 2019
	Version   :

	Notes     :
	Permet de personnaliser des 'Widgets' GtkButton et GtkMenuItem, de leur
	affecter des images et de contrôler leur position.

	Révisions :

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#pragma once

#include <gtk/gtk.h>


GtkWidget*
create_img_button(GtkWidget *img, const gchar *text);
const gchar*
img_button_get_text(GtkWidget *img_button);

GtkWidget*
create_img_menu_item(GtkWidget *image, const gchar *text);
const gchar*
img_menu_item_get_text(GtkWidget *img_menu_item);
