#include "etech_utils.h"


//// Privé.


// Retrouver le Widget GtkLabel (afin d'en extraire éventuellement le texte).
static GtkWidget*
get_btn_or_menu_item_label(GtkWidget *parent)
{
	GList *list = NULL;
	GtkWidget *hbox = NULL, *label = NULL;

	/*
	 * Le GtkBox est le seul Widget enfant puisque GtkMenuItem et GtkButton sont
	 * de type GTK_BIN.
	 */
	list = gtk_container_get_children(GTK_CONTAINER(parent));
	hbox = g_list_nth_data(list, 0);
	g_list_free(list);
	if ( ! hbox ) {
		g_print("ERREUR : ! hbox\n");

		return NULL;
	}

	/*
	 * Le GtkLabel est le second enfant du GtkBox (voir create_img_button et
	 * create_img_menu_item).
	 */
	list = gtk_container_get_children(GTK_CONTAINER(hbox));
	label = g_list_nth_data(list, 1);
	g_list_free(list);
	if ( ! label || ! GTK_IS_LABEL(label) ) {
		g_print("ERREUR : ! label\n");

		return NULL;
	}

	return label;
}


//// Public.


/*
 * Création d'un bouton personnalisé avec image.
 * Fonction pour retrouver son texte.
 * GtkButton (GtkBin)
 *          > GtkBox
 * 					> GtkImage | GtkLabel.
 */
GtkWidget*
create_img_button(GtkWidget *img, const gchar *text)
{
	GtkWidget *img_btn = gtk_button_new();
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

	gtk_box_pack_start(GTK_BOX(hbox), img, FALSE, FALSE, 4);
	GtkWidget *label = gtk_label_new(text);
	gtk_label_set_xalign(GTK_LABEL(label), 0.0);
	gtk_box_pack_end(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(img_btn), hbox);

	// Nom donné pour un éventuel style CSS (ex. #imgbtn{padding: 0px 0px;}).
	gtk_widget_set_name(img_btn, "imgbtn");

	return img_btn;
}

const gchar*
img_button_get_text(GtkWidget *img_button)
{
	GtkWidget *label = get_btn_or_menu_item_label(img_button);
	if ( ! label ) {
		return NULL;
	}

	return gtk_label_get_text(GTK_LABEL(label));
}

/*
 * Ligne de menu avec image.
 * Fonction pour retrouver son texte.
 * GtkMenuItem (GtkBin)
 *          	> GtkBox
 * 						> GtkImage | GtkLabel.
 */
GtkWidget*
create_img_menu_item(GtkWidget *image, const gchar *text)
{
    GtkWidget *mi = gtk_menu_item_new();
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 6);
    GtkWidget *label = gtk_label_new(text);
    gtk_widget_set_margin_end(label, 6);

	gtk_container_add(GTK_CONTAINER(hbox), 	image);
	gtk_container_add(GTK_CONTAINER(hbox), 	label);
	gtk_container_add(GTK_CONTAINER(mi), 	hbox);

    return mi;
}

const gchar*
img_menu_item_get_text(GtkWidget *img_menu_item)
{
	GtkWidget *label = get_btn_or_menu_item_label(img_menu_item);
	if ( ! label ) {
		return NULL;
	}

	return gtk_label_get_text(GTK_LABEL(label));
}
