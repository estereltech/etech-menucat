#include "options.h"
#include "etech_utils.h"
#include "etechappwin.h"


//// Privé.


#define MARGIN 			4
#define SCALE_WIDTH		200

/*
 * Structure contenant les données privées du dialogue.
 * Ces données peuvent être fournies par des fonctions accesseurs publiques.
 */
typedef struct _ETechOptionsDialogPrivate ETechOptionsDialogPrivate;
struct _ETechOptionsDialogPrivate {

	// Principaux widgets.
	GtkWidget *label_left, *label_top, *label_term;
	GtkWidget *scale_left, *scale_top, *entry_term;

	// Configuration passée par la fenêtre principale.
	Config cfg;

	// Coordonnées du menu 'CatMenu'.
	gint ileft;
	gint itop;

	// Commande de l'émulateur de terminal à utiliser.
	gchar *terminal_command;

	// Dimensions de l'écran principal.
	GdkRectangle screen_rectangle;
};

/*
 * Structure du dialogue.
 * Héritage de 'GtkDialog'.
 */
struct _ETechOptionsDialog {
    GtkDialog parent;
};

/*
 * Structure de la classe de dialogue.
 * Héritage de 'GtkDialogClass'.
 */
struct _ETechOptionsDialogClass {
    GtkDialogClass parent_class;
};

G_DEFINE_TYPE_WITH_PRIVATE(ETechOptionsDialog, etech_options_dialog,
															GTK_TYPE_DIALOG);

static void
etech_options_dialog_constructed(GObject *object)
{
    // g_print("2 - etech_options_dialog_constructed\n");

    G_OBJECT_CLASS(etech_options_dialog_parent_class)->constructed(object);
}

static void
etech_options_dialog_dispose(GObject *object)
{
#if 0
	static gint times = 0;
	times++;
    g_print("4 - etech_options_dialog_dispose - %d\n", times);
#endif
    G_OBJECT_CLASS(etech_options_dialog_parent_class)->dispose(object);
}

static void
etech_options_dialog_finalize(GObject *object)
{
    //g_print("5 - etech_options_dialog_finalize\n");

    G_OBJECT_CLASS(etech_options_dialog_parent_class)->finalize(object);
}

static void
etech_options_dialog_class_init(ETechOptionsDialogClass __unused *class)
{
	G_OBJECT_CLASS(class)->constructed 	= etech_options_dialog_constructed;
    G_OBJECT_CLASS(class)->dispose 		= etech_options_dialog_dispose;
	G_OBJECT_CLASS(class)->finalize 	= etech_options_dialog_finalize;
}

static void
etech_options_btn_ok_cb(GtkWidget __unused *sender, gpointer __unused data)
{
	// Pour accéder au membre privé 'priv->cfg'.
	ETechOptionsDialogPrivate *priv =
							etech_options_dialog_get_instance_private(data);

	// Enregistrement éventuel de la clé 'term_cmd'.
	const gchar *tcmd = gtk_entry_get_text(GTK_ENTRY(priv->entry_term));
	if ( g_strcmp0(tcmd, priv->terminal_command) != 0 ) {
		if ( ! config_set_terminal_command(priv->cfg, tcmd) ) {
			g_print("Erreur : ! config_set_terminal_command\n");
		}
	}

	/*
	 * Enregistrement d'une ou deux coordonnées dans la configuration si une
	 * ou deux ont été modifiées.
	 */
	gint new_ileft = (gint)gtk_range_get_value(GTK_RANGE(priv->scale_left));
	gint new_itop = (gint)gtk_range_get_value(GTK_RANGE(priv->scale_top));
	if ( new_ileft != priv->ileft && new_itop != priv->itop ) {
		if ( ! config_set_menu_coords(priv->cfg, new_ileft, new_itop) ) {
			g_print("Erreur : ! config_set_menu_coords\n");
		}

		g_print("Les deux coordonnées X et Y ont été modifiées.\n");
	} else {
		if ( new_ileft != priv->ileft ) {
			if ( ! config_set_menu_left(priv->cfg, new_ileft) ) {
				g_print("Erreur : ! config_set_menu_left\n");
			}

			g_print("La coordonnée X a été modifiée.\n");
		}

		if ( new_itop != priv->itop ) {
			if ( ! config_set_menu_top(priv->cfg, new_itop) ) {
				g_print("Erreur : ! config_set_menu_top\n");
			}

			g_print("La coordonnée Y a été modifiée.\n");
		}
	}
}

static void
option_css_error_cb(GtkCssProvider __unused *provider,
								GtkCssSection *section,
								const GError *error, GtkTextBuffer *buffer)
{
	GtkTextIter start, end;
	const char *tag_name;

	gtk_text_buffer_get_iter_at_line_index(buffer,
								&start,
                                gtk_css_section_get_start_line(section),
                                gtk_css_section_get_start_position(section));
	gtk_text_buffer_get_iter_at_line_index(buffer,
                                &end,
                                gtk_css_section_get_end_line(section),
                                gtk_css_section_get_end_position(section));

	if ( g_error_matches(error, GTK_CSS_PROVIDER_ERROR,
										GTK_CSS_PROVIDER_ERROR_DEPRECATED) ) {
		tag_name = "avertissement";
	} else {
		tag_name = "erreur";
	}

	gtk_text_buffer_apply_tag_by_name(buffer, tag_name, &start, &end);
}

static void
etech_options_dialog_init(ETechOptionsDialog __unused *dlg)
{
	//g_print("1 - etech_options_dialog_init\n");

	GtkWidget *vbox;
    GtkWidget *grid;

    ETechOptionsDialogPrivate *priv =
							etech_options_dialog_get_instance_private(dlg);

	// Ajout de styles CSS.
    GtkCssProvider *css;
    GdkDisplay *display;
    GdkScreen *screen;
    const gchar *css_data = "#etech_vbox{padding:8px;"
							"border:1px solid #858585;"
							"border-radius:4px;color:black;"
							"background-color:#fff;"
							"box-shadow:inset 1px 1px 2px 0px rgba(0,0,0,0.5);}"
							"#etech_scale value{color:#024;font-size:12px;"
							"margin-bottom:0px;}";
    css = gtk_css_provider_new();
    display = gdk_display_get_default();
    screen = gdk_display_get_default_screen(display);
	gtk_style_context_add_provider_for_screen(screen,
		GTK_STYLE_PROVIDER(css),
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	gtk_css_provider_load_from_data(css, css_data, -1, NULL);
    g_signal_connect(css, "parsing-error",
									G_CALLBACK(option_css_error_cb), NULL);
    g_object_unref(css);


	/*
	 * Dimensions du moniteur principal (celui du Bureau) de l'affichage en
	 * pixels stockées dans une donnée privée.
	 * Les dimensions sont utilisée pour les widgets GtkScale, ci-dessous.
	 */
	display = gdk_display_get_default();
	GdkMonitor *monitor = gdk_display_get_primary_monitor(display);
	gdk_monitor_get_workarea(monitor, &priv->screen_rectangle);

    // Obtention du conteneur principal.
    GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG(dlg));

    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_set_name(vbox, "etech_vbox");
	gtk_container_add(GTK_CONTAINER(content_area), vbox);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), MARGIN);

    // Grille (deux colonnes).
    grid = gtk_grid_new();
    gtk_box_pack_start(GTK_BOX(vbox), grid, TRUE, TRUE, 0);

	priv->label_left = gtk_label_new("Position horizontale :");
	gtk_label_set_xalign(GTK_LABEL(priv->label_left), 0.0);
	gtk_widget_set_margin_start(priv->label_left, MARGIN);
	gtk_grid_attach(GTK_GRID(grid), priv->label_left, 0, 1, 1, 1);
	priv->scale_left = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL,
												0,
												priv->screen_rectangle.width,
												1);
	gtk_widget_set_name(priv->scale_left, "etech_scale");
	gtk_scale_set_draw_value(GTK_SCALE(priv->scale_left), TRUE);
	gtk_widget_set_size_request(priv->scale_left, SCALE_WIDTH, -1);
	gtk_scale_set_value_pos(GTK_SCALE(priv->scale_left), GTK_POS_RIGHT);
	gtk_grid_attach(GTK_GRID(grid), priv->scale_left, 1, 1, 1, 1);

	priv->label_top = gtk_label_new("Position verticale :");
	gtk_label_set_xalign(GTK_LABEL(priv->label_top), 0.0);
	gtk_widget_set_margin_start(priv->label_top, MARGIN);
	gtk_grid_attach(GTK_GRID(grid), priv->label_top, 0, 2, 1, 1);
	priv->scale_top = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL,
												0,
												priv->screen_rectangle.height,
												1);
	gtk_widget_set_name(priv->scale_top, "etech_scale");
	gtk_scale_set_draw_value(GTK_SCALE(priv->scale_top), TRUE);
	gtk_widget_set_size_request(priv->scale_top, SCALE_WIDTH, -1);
	gtk_scale_set_value_pos(GTK_SCALE(priv->scale_top), GTK_POS_RIGHT);
	gtk_grid_attach(GTK_GRID(grid), priv->scale_top, 1, 2, 1, 1);

	// Commande pour un émulateur de terminal.
	priv->label_term = gtk_label_new("Commande de Terminal :");
	gtk_label_set_xalign(GTK_LABEL(priv->label_term), 0.0);
	gtk_widget_set_margin_start(priv->label_term, MARGIN);
	gtk_grid_attach(GTK_GRID(grid), priv->label_term, 0, 3, 1, 1);
	priv->entry_term = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(priv->entry_term), 64);
	gtk_entry_set_width_chars(GTK_ENTRY(priv->entry_term), 16);
	gtk_widget_set_margin_start(priv->entry_term, MARGIN * 3);
	gtk_widget_set_margin_end(priv->entry_term, MARGIN * 3);
	gtk_widget_set_margin_bottom(priv->entry_term, MARGIN);
	gtk_grid_attach(GTK_GRID(grid), priv->entry_term, 1, 3, 1, 1);

    // Bouton 'Annuler' et marge droite.
	GtkWidget *img_cancel = gtk_image_new_from_resource("/icons/cancel.png");
	GtkWidget *etech_btn_cancel = create_img_button(img_cancel, "Annuler");
	gtk_dialog_add_action_widget(GTK_DIALOG(dlg),
								etech_btn_cancel,
								GTK_RESPONSE_CANCEL);
	gtk_widget_set_margin_end(etech_btn_cancel, MARGIN / 2);

	// Bouton 'Valider'.
	GtkWidget *img_ok = gtk_image_new_from_resource("/icons/ok.png");
	GtkWidget *etech_btn_ok = create_img_button(img_ok, "Valider");
	gtk_dialog_add_action_widget(GTK_DIALOG(dlg),
								etech_btn_ok,
								GTK_RESPONSE_OK);

	// Si validation, modifier la configuration dans le gestionnaire du signal.
	g_signal_connect(etech_btn_ok, "clicked",
								G_CALLBACK(etech_options_btn_ok_cb), dlg);

	// Ajout d'une classe au contexte de style.
	GtkStyleContext *context = gtk_widget_get_style_context(etech_btn_ok);
	gtk_style_context_add_class(context, "suggested-action");

	// Affichage des widgets créés et marge.
    gtk_widget_show_all(content_area);
    gtk_container_set_border_width(GTK_CONTAINER(content_area), MARGIN);
}


//// Public.


GtkWidget*
etech_options_dialog_new(Config cfg)
{
	// g_print("3 - etech_options_dialog_new\n");

    ETechOptionsDialog *dlg = g_object_new(ETECH_OPTIONS_DIALOG_TYPE, NULL);

    ETechOptionsDialogPrivate *priv =
							etech_options_dialog_get_instance_private(dlg);

	// Affectation de la configuration passée en argument.
	priv->cfg = cfg;

	// Obtention des coordonnées du menu par la configuration.
	priv->ileft = 0;
	priv->itop = 0;
	config_get_menu_coords(priv->cfg, &priv->ileft, &priv->itop);

	gtk_range_set_value(GTK_RANGE(priv->scale_left), (gdouble)priv->ileft);
	gtk_range_set_value(GTK_RANGE(priv->scale_top), (gdouble)priv->itop);

	priv->terminal_command = config_get_terminal_command(priv->cfg);
	if ( priv->terminal_command ) {
		gtk_entry_set_text(GTK_ENTRY(priv->entry_term), priv->terminal_command);
	} else {
		g_print("! priv->terminal_command\n");
	}

    /*
     * Empêcher le redimensionnement du dialogue.
     * Ceci est fait après 'etech_dialog_init'.
     */
    gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);

    return GTK_WIDGET(dlg);
}

