#include <gtk/gtk.h>

#include "etechapp.h"
#include "etechappwin.h"
#include "package_version.h"


struct _ETechApp {
    GtkApplication parent;
};


struct _ETechAppClass {
    GtkApplicationClass parent_class;
};

typedef struct _ETechAppPrivate ETechAppPrivate;
struct _ETechAppPrivate {
	GdkPixbuf *logo;
	GdkPixbuf *icon;
};

G_DEFINE_TYPE_WITH_PRIVATE(ETechApp, etech_app, GTK_TYPE_APPLICATION);


static void
etech_app_init(ETechApp *app)
{
    g_print("etech_app_init\n");

    // Accès à la structure des données privées.
	ETechAppPrivate *priv = etech_app_get_instance_private(ETECH_APP(app));

	// Icône et logo de l'application.
	priv->icon = gdk_pixbuf_new_from_resource("/icons/cat_16.png", NULL);
	priv->logo = gdk_pixbuf_new_from_resource("/icons/cat_96.png", NULL);
}

static void
etech_app_shutdown(GApplication __unused *app)
{
	g_print("etech_app_shutdown\n");

    G_APPLICATION_CLASS(etech_app_parent_class)->shutdown(app);
}

static void
etech_app_startup(GApplication *app)
{
    G_APPLICATION_CLASS(etech_app_parent_class)->startup(app);

    g_print("etech_app_startup\n");
}

static void
etech_app_activate(GApplication *app)
{
	g_print("etech_app_activate\n");

    ETechAppWindow *win = etech_app_window_new(ETECH_APP(app));
    ETechAppPrivate *priv = etech_app_get_instance_private(ETECH_APP(app));

    // Icône principale de l'application.
	gtk_window_set_icon(GTK_WINDOW(win), priv->icon);

    gtk_window_present_with_time(GTK_WINDOW(win), GDK_CURRENT_TIME);
}

static void
etech_app_constructed(GObject *object)
{
	G_OBJECT_CLASS(etech_app_parent_class)->constructed(object);

	g_print("etech_app_constructed\n");
}

static void
etech_app_dispose(GObject *object)
{
	g_print("etech_app_dispose\n");

	// Accès à la structure des données privées.
	ETechAppPrivate *priv = etech_app_get_instance_private(ETECH_APP(object));

    if ( priv->logo ) {
		g_print("g_clear_object(priv->logo)\n");
		g_clear_object(&priv->logo);
    }

    if ( priv->icon ) {
		g_print("g_clear_object(priv->icon)\n");
		g_clear_object(&priv->icon);
	}

    G_OBJECT_CLASS(etech_app_parent_class)->dispose(object);
}

static void
etech_app_finalize(GObject *object)
{
	g_print("etech_app_finalize\n");

    G_OBJECT_CLASS(etech_app_parent_class)->finalize(object);
}

static void
etech_app_class_init(ETechAppClass *class)
{
	// GApplication.
    G_APPLICATION_CLASS(class)->startup 	= etech_app_startup;
    G_APPLICATION_CLASS(class)->activate 	= etech_app_activate;
    G_APPLICATION_CLASS(class)->shutdown 	= etech_app_shutdown;

    // GObject.
    G_OBJECT_CLASS(class)->constructed 		= etech_app_constructed;
    G_OBJECT_CLASS(class)->dispose 			= etech_app_dispose;
    G_OBJECT_CLASS(class)->finalize 		= etech_app_finalize;
}

ETechApp*
etech_app_new(void)
{
    ETechApp *app = g_object_new(ETECH_APP_TYPE,
                                 "application-id", APPLICATION_ID,
                                 "flags", G_APPLICATION_FLAGS_NONE,
                                 NULL);

    return app;
}

GdkPixbuf*
etech_app_get_logo(ETechApp *app)
{
	// Accès à la structure des données privées.
	ETechAppPrivate *priv = etech_app_get_instance_private(ETECH_APP(app));

	return priv->logo;
}

GdkPixbuf*
etech_app_get_icon(ETechApp *app)
{
	// Accès à la structure des données privées.
	ETechAppPrivate *priv = etech_app_get_instance_private(ETECH_APP(app));

	return priv->icon;
}

